package com.nopcommerce;

public class HomePageUI {
	//Header
	public static final String HEADER_REGISTER_LINK = "//a[@class='ico-register']";
	public static final String HEADER_LOGIN_LINK = "//a[@class='ico-login']";
	public static final String HEADER_LOGOUT_LINK = "//a[@class='ico-logout']";
	public static final String HEADER_LOGO = "//div[@class='header-logo']//a/img";
	
	//Header
	public static final String HEADER_ACCOUNT_LINK="//a[@class='ico-account']";
	public static final String HEADER_WISHLIST ="//span[contains(text(),'Wishlist')]";
	public static final String HEADER_SHOPPING_CART =" //span[@class='cart-label']";
	
	//Menu Bar
	public static final String DYNAMIC_ELEMENT_CATEGORY = "//ul[@class='top-menu notmobile']//a[contains(.,'%s')]";
	public static final String DYNAMIC_ELEMENT_CHILD_CATEGORY = "//ul[@class='top-menu notmobile']//a[contains(text(),'%s']";
	
	//Footer
	public static final String FOOTER_DYNAMIC_CATEGORY = "//a[text()='%s']";
}
