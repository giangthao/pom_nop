package com.nopcommerce;

public class RegisterPageUI {
	public static final String GENDER_MALE_RADIO = "//input[@id='gender-male']";
	public static final String GENDER_FEMALE_RADIO = "//input[@id='gender-female']";
	public static final String LAST_NAME_TEXTBOX="//input[@id='LastName']";
	public static final String FIRST_NAME_TEXTBOX="//input[@id='FirstName']";
	public static final String DAY_OF_BIRTH="//a[@class='ico-account']";
	public static final String MONTH_OF_BIRTH="//a[@class='ico-account']";
	public static final String YEAR_OF_BIRTH="//a[@class='ico-account']";
	public static final String EMAIL_TEXTBOX="////input[@id='Email']";
	public static final String COMPANY_TEXTBOX=" //input[@id='Company']";
	public static final String PASSWORD_TEXTBOX="//input[@id='Password']";
	public static final String CONFIRMPASSWORD_TEXTBOX="//input[@id='Password']";
	public static final String NEWSLETTER_CHECKBOX="//input[@id='Newsletter']";
	public static final String REGISTER_BUTTON="//button[@id='register-button']";
	//Dynamic Message
	public static final String MESSAGE_ERROR_DYNAMIC="//span[@id='%s' and text()='%s']";
	//Message Empty
	public static final String MESSAGE_FIRST_NAME_ERROR="//span[@id='%s' and text()='%s']";
	public static final String DYNAMIC_TEXTBOX ="input[@id='%s']";

}
