package com.nopcomerce.user;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.nopcommerce.RegisterPageUI;

import commons.AbstractTest;
import nopCommerce.pageObjects.HomePageObjects;
import nopCommerce.pageObjects.PageGeneratorManager;
import nopCommerce.pageObjects.RegisterPageObjects;

public class FE_01_Register extends AbstractTest{
	public WebDriver driver;
	private HomePageObjects homePage;
	private RegisterPageObjects registerPage;
	String email;
	String firstnameError, lastnameError, emailError ;
	String firstName, lastName, companyName,password, confirmpassword;
	@Parameters("browser")
	@BeforeTest
	public void beforeClass(String browserName) {
		driver = OpenMultiBrowsers(browserName);
		firstName = "Ermina";
		lastName = "MacCahey";
		companyName = "MacCahey";
		password = "MacCahey";
		confirmpassword = "";
		
	}
	@Test
	public void TC_01_Register_With_Empty_Data() throws InterruptedException {
		log.info("TC_01_Register with empty Data");
		homePage = PageGeneratorManager.getHomePage(driver);
		
		log.info("TC_01_Register with empty Data - Step 01 - Open The Register page");
		homePage.openRegisterPage();
		registerPage = PageGeneratorManager.getRegisterPage(driver);
		
		log.info("TC_01_Register with empty Data - Step 02 - Dont Input Any Field");
		registerPage.clickButtonRegister();
		/*
		 * Thread.sleep(3000); log.info("Verify Message Displayed error");
		 * verifyTrue(registerPage.isRegisterMessageDisplayed());
		 * log.info("Display message true but verify False");
		 */
		log.info("TC_01_Register with empty Data - Step 03 - Verify Error Message");
		verifyTrue(registerPage.isDynamicMessage(RegisterPageUI.MESSAGE_ERROR_DYNAMIC, "FirstName-error", "First name is required."));
		verifyTrue(registerPage.isDynamicMessage(RegisterPageUI.MESSAGE_ERROR_DYNAMIC, "LastName-error", "Last name is required."));
		verifyTrue(registerPage.isDynamicMessage(RegisterPageUI.MESSAGE_ERROR_DYNAMIC, "Email-error", "Email is required."));
		verifyTrue(registerPage.isDynamicMessage(RegisterPageUI.MESSAGE_ERROR_DYNAMIC, "Password-error", "Password is required."));
		verifyTrue(registerPage.isDynamicMessage(RegisterPageUI.MESSAGE_ERROR_DYNAMIC, "ConfirmPassword-error", "Password is required."));
		
		
	}
	@Test
	public void TC_02_Register_With_Invalid_Email() {
		log.info("TC_02_Register with Invalid Email");
		
	}
	@Test
	public void TC_03_Register_With_Exist_Email() {
		
	}
	@Test
	public void TC_04_Register_With_SixCharacter_Password() {
		
	}
	@Test
	public void TC_05_Register_With_Password_Not_Match() {
		
	}
	
	@Test
	public void TC_06_Register_With_Valid_Data() {
		
	}
	@AfterTest
	public void quitDriver() {
		closeBrowserAndDriver(driver);
	}
	
}
