package com.nopcomerce.user;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import commons.AbstractTest;
import nopCommerce.pageObjects.HomePageObjects;
import nopCommerce.pageObjects.PageGeneratorManager;
import reportConfig.ExtentTestManager;

public class TC_01_HomePage extends AbstractTest {
	public WebDriver driver;
	private HomePageObjects homePageOj;

	@Parameters("browser")

	@BeforeTest
	public void beforeClass(String browserName) {
		driver = OpenMultiBrowsers(browserName);

	}
	@Test
	public void TC_01_checkCategoryDisplay(Method method) {
		ExtentTestManager.startTest(method.getName(),"TC_01 start");
		homePageOj = PageGeneratorManager.getHomePage(driver);
		
		verifyTrue(homePageOj.isCategoryDisplay("Computers"));
		verifyTrue(homePageOj.isCategoryDisplay("Electronicss"));
		verifyTrue(homePageOj.isCategoryDisplay("Apparel"));
		verifyTrue(homePageOj.isCategoryDisplay("Digital downloads"));
		verifyTrue(homePageOj.isCategoryDisplay("Books"));
		verifyTrue(homePageOj.isCategoryDisplay("Jewelry"));
		verifyTrue(homePageOj.isCategoryDisplay("Gift Cards"));
		// verifyTrue(homePageOj.isCategoryDisplay("Computers"));
		ExtentTestManager.endTest();
	}

	
	public void TC_02_clicktoChildElement() {
		homePageOj = PageGeneratorManager.getHomePage(driver);
		homePageOj.clickToChildCategory("Computers", "Desktops");
		homePageOj.clickToChildCategory("Computers", "Notebooks");
		homePageOj.clickToChildCategory("Computers", "Software");
		homePageOj.clickToChildCategory("Electronics", "Cell phones");
		homePageOj.clickToChildCategory("Electronics", "Camera");
		homePageOj.clickToChildCategory("Electronics", "Others");
		homePageOj.clickToChildCategory("Electronics", "Others");
		homePageOj.clickToChildCategory("Apparel", "Shoes");
		homePageOj.clickToChildCategory("Apparel", "Clothing");
		homePageOj.clickToChildCategory("Apparel", "Accessories");

	}
	
	public void TC_03_checkCategoryDisplay() throws InterruptedException {
		homePageOj = PageGeneratorManager.getHomePage(driver);
		homePageOj.clickToChildCategory("Electronics", "Cell phones");
		verifyFalse(homePageOj.isCategoryDisplay("Computers"));
		System.out.println("ABC");
	}
	@Test
	public void TC_04_checkCategoryDisplay() {
		log.info("Start Testcase Fail at Here");
		verifyFalse(true);
		verifyTrue(false);
		log.info("Print at here");
	}
	

	@AfterTest
	public void quit() {
		closeBrowserAndDriver(driver);
	}
}
