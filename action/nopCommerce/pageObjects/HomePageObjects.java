package nopCommerce.pageObjects;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.nopcommerce.HomePageUI;

import commons.AbstractPageObject;

public class HomePageObjects extends AbstractPageObject {
	WebDriver driverGlobal;
	public HomePageObjects(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
		driverGlobal = driver;
	}
	public boolean isCategoryDisplay(String locators) {
		// TODO Auto-generated method stub
		//System.out.println(castRestParameter(HomePageUI.DYNAMIC_ELEMENT_CATEGORY,locators));
	//	waitToElementVisible(" //ul[@class='top-menu notmobile']//a[normalize-space()='Computerss']");
		return isElementDisplayed(HomePageUI.DYNAMIC_ELEMENT_CATEGORY,locators);
	}
	public void clickToChildCategory(String locators,String values) {
		hoverMouseToElement("//span[@id='Email-error' and text()='Email is required.']");
		clickToElement(HomePageUI.DYNAMIC_ELEMENT_CATEGORY,values);
	}
	public void  openRegisterPage() {
		// TODO Auto-generated method stub
		waitToElementVisible(HomePageUI.HEADER_REGISTER_LINK);
		clickToElement(HomePageUI.HEADER_REGISTER_LINK);
	}

	
}
