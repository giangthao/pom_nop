package nopCommerce.pageObjects;

import org.openqa.selenium.WebDriver;

import com.nopcommerce.RegisterPageUI;

import commons.AbstractPageObject;

public class RegisterPageObjects extends AbstractPageObject{

	public RegisterPageObjects(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public boolean isRegisterMessageDisplayed() {
		waitToElementVisible("//span[@id='FirstName-error' and text() = 'First name is required.']" );
		return isElementDisplayed("//span[@id='FirstName-error' and text() = 'First name is required.']");
	}
	public boolean isDynamicMessage(String locator, String value, String values) {
		waitToElementVisible("//span[@id='FirstName-error' and text() = 'First name is required.']" );
		return isElementDisplayed(locator, value,values);
		
	}
	public void clickButtonRegister() {
		// TODO Auto-generated method stub
		waitToElementVisible(RegisterPageUI.REGISTER_BUTTON);
		clickToElement(RegisterPageUI.REGISTER_BUTTON);
	}
	public void inputToTextbox(String locator,String value) {
		// TODO Auto-generated method stub
		
	}

}
