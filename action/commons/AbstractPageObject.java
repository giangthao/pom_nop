package commons;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nopcommerce.AbstractPageUI;



public class AbstractPageObject {
	WebElement element;
	By by;
	Select select;
	JavascriptExecutor javascriptExecutor;
	WebDriverWait webDriverWait;
	List<WebElement> elements;
	WebDriverWait waitExplicit;
	Set<String> allwindows;
	Actions action;
	Alert alert;
	long shortTimeout = 5;
	long longTimeout = 30;
	WebDriver driver;
	public AbstractPageObject(WebDriver driver){
		this.driver = driver;
		action = new Actions(driver);
		javascriptExecutor = (JavascriptExecutor) driver;
		waitExplicit = new WebDriverWait(driver, longTimeout);
	}
	/* WEB BROWSER */
	public void openUrl(String url) {
		driver.get(url);
	}
	public String getPageTitle() {
		return driver.getTitle();
	}
	public String getCurrentPageUrl() {
		return driver.getCurrentUrl();
	}
	public void backToPage() {
		driver.navigate().back();
	}
	public void forwardToPage() {
		driver.navigate().forward();
	}
	public void acceptAlert() {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		waitExplicit.until(ExpectedConditions.alertIsPresent());
		alert = driver.switchTo().alert();

		alert.accept();
	}
	public void cancelAlert() {
		driver.switchTo().alert().dismiss();
	}

	public String getTextAlert() {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		waitExplicit.until(ExpectedConditions.alertIsPresent());
		return driver.switchTo().alert().getText();
	}

	public void sendKeyToAlert(String value) {
		driver.switchTo().alert().sendKeys(value);
	}
	/* WEB ELEMENTS */

	public void clickToElement(String locator) {
		element = find(locator);
		highlightElement(element);
		if (driver.toString().contains("internet")) {
			clickToElementbyJS(locator);
			sleepInSecond(5);
		} else {
			element.click();
		}

	}
	public void clickToElement(String locator,String ...values) {
		locator = castRestParameter(locator, values);
		element = find(locator);
		highlightElement(element);
		if (driver.toString().contains("internet")) {
			clickToElementbyJS(locator);
			sleepInSecond(5);
		} else {
			element.click();
		}

	}
	public void uploadMultipleFiles( String ...fileNames) {
		String filePath = System.getProperty("user.dir")+"\\uploadFiles\\";
		String fullFileName ="";
		for(String file:fileNames) {
			fullFileName = fullFileName + filePath + file +"\n";
			
		}
		fullFileName = fullFileName.trim();
		sendKeyToElement(AbstractPageUI.UPLOAD_FILE_TYPE, fullFileName);
	}
	public void sendKeyToElement(String locator, String valueToSendkey, String... values) {
		locator = castRestParameter(locator, values);
		
		element = find(locator);
		highlightElement(element);
		element.clear();
		element.sendKeys(valueToSendkey);
	}
	public void sendKeyToElement(String locator, String value) {
		element = find(locator);
		highlightElement(element);
		element.clear();
		element.sendKeys(value);
	}
	public void selectItemInDropdown(String locator, String valueItem) {
		element = find(locator);
		highlightElement(element);
		select = new Select(element);
		select.selectByVisibleText(valueItem);
	}
	public void selectItemInDropdown(String locator, String valueItem, String... values) {
		locator = castRestParameter(locator, values);
		element = find(locator);
		highlightElement(element);
		select = new Select(element);
		select.selectByVisibleText(valueItem);
		// driver.switchTo().frame(element);
	}
	public String getFirstItemInDropdown(String locator) {
		select = new Select(driver.findElement(By.xpath(locator)));
		return select.getFirstSelectedOption().getText();
	}

	public void selectItemInCustomDropdown(String parentLocator, String allItemsLocator, String expectedItem) {
		javascriptExecutor = (JavascriptExecutor) driver;
		waitExplicit = new WebDriverWait(driver, longTimeout);

		WebElement element = driver.findElement(By.xpath(parentLocator));
		javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", element);
		javascriptExecutor.executeScript("arguments[0].click();", element);

		waitExplicit.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(allItemsLocator)));

		elements = driver.findElements(By.xpath(allItemsLocator));
		for (WebElement item : elements) {
			if (item.getText().equals(expectedItem)) {
				javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", item);
				sleepInSecond(1);
				item.click();
				sleepInSecond(1);
				break;
			}
		}
	}
	public String getAttributeValue(String locator, String attributeName) {
		element = find(locator);
		highlightElement(element);
		return element.getAttribute(attributeName);
	}
	public String getTextElement(String locator) {
		element =find(locator);
		highlightElement(element);
		return element.getText();
	}
	public String getTextElement(String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
		element =find(locator);
		highlightElement(element);
		return element.getText();
	}

	public int countNumberOfElement(String locator) {
		element =find(locator);
		highlightElement(element);
		return elements.size();
	}
	public boolean isElementDisplayed(String locator) {
		overideGlobalTimeout(shortTimeout);
		try {
			element = find(locator);
			overideGlobalTimeout(longTimeout);
			return element.isDisplayed();
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			overideGlobalTimeout(longTimeout);
			return false;
		}}
	public void overideGlobalTimeout(long timeout) {
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
	}
	public boolean isElementDisplayed(String locator, String... values) {
		locator = castRestParameter(locator, values);
		overideGlobalTimeout(shortTimeout);
		try {
			element = find(locator);
			overideGlobalTimeout(longTimeout);
			return element.isDisplayed();
		} catch (Exception e) {
			// TODO: handle exception
			overideGlobalTimeout(longTimeout);
			return false;
		}

	}
	public void waitToElementVisible(String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
		by = By.xpath(locator);
		waitExplicit = new WebDriverWait(driver, longTimeout);
		waitExplicit.until(ExpectedConditions.visibilityOfElementLocated(by));
		// System.out.println(by);
	}
	public boolean isElementSelected(String locator) {
		element =find(locator);
		highlightElement(element);
		return element.isSelected();
	}
	public boolean isElementEnabled(String locator) {
		element = driver.findElement(By.xpath(locator));
		return element.isEnabled();
	}

	public void checkToCheckBox(String locator) {
		element = find(locator);
		highlightElement(element);
		if (element.isSelected() == false) {
			element.click();
		}
	}

	public void unCheckToCheckBox(String locator) {
		element = find(locator);
		highlightElement(element);
		if (element.isSelected() == true) {
			element.click();
		}
	}
	public void hoverMouseToElement(String locator) {
		element = driver.findElement(By.xpath(locator));
		highlightElement(element);
		action.moveToElement(element).perform();
	}
	public void hoverMouseToElement(String locator, String ...values) {
		locator = castRestParameter(locator, values);
		element = driver.findElement(By.xpath(locator));
		highlightElement(element);
		action.moveToElement(element).perform();
	}
	public String castRestParameter(String locator, String... value) {
		locator = String.format(locator, (Object[]) value);
		return locator;
	}
	public void highlightElement(WebElement element) {
		javascriptExecutor = (JavascriptExecutor) driver;
		String originalStyle  = element.getAttribute("style");
		//System.out.println("Original style of element = "+originalStyle);
		javascriptExecutor.executeScript("arguments[0].setAttribute(arguments[1], arguments[2])", element, "style", "border: 3px solid red; border-style: dashed;");
		sleepInSecond(1);
		javascriptExecutor.executeScript("arguments[0].setAttribute(arguments[1], arguments[2])", element, "style", originalStyle);
	}
	public WebElement find(String locator) {
		return driver.findElement(By.xpath(locator));
	}
	public void sleepInSecond(long numberInSecond) {
		try {
			Thread.sleep(numberInSecond * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void clickToElementbyJS(String locator) {
		element = find(locator);
		highlightElement(element);
		javascriptExecutor = (JavascriptExecutor) driver;
		javascriptExecutor.executeScript("arguments[0].click();", element);

	}
}
